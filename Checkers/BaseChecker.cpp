#include "BaseChecker.h"

void Checker::AddText(const char *text) {
    in.open(text);
    try {
        if (!in.is_open()) {
            throw std::invalid_argument(std::string(text) + " not open");
        }
    } catch (...) {
        global_ex = std::current_exception();
    }
    std::string line;
    std::string word;
    size_t offset = 0;

    parameters.check_time.Start();
    while (getline(in, line)) {
        if (!line.empty() && !Parser::LineCleaner(line)) {
            do {
                word = Parser::WordParser(line, offset);
                check(word);
            } while (offset != 0);
        }
    }
    parameters.check_time.Stop();

    in.close();
}

void Checker::AddVocabulary(const char *voc) {
    in.open(voc);
    try {
        if (!in.is_open()) {
            throw std::invalid_argument("Vocabulary not open");
        }
    } catch (...) {
        global_ex = std::current_exception();
    }
    std::string str;

    parameters.vocabulary_download_time.Start();
    while (getline(in, str)) {
        load(str);
    }
    parameters.vocabulary_download_time.Stop();

    in.close();
}
