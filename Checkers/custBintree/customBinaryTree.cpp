#include "customBinaryTree.h"

void customBinaryTreeChecker::check(std::string &str) {
    ++parameters.sum_check_word;
    if (!_vocabulary->find(str)) {
        _non_find->Add(str);
        ++parameters.sum_uncorrect_word;
    }
}

void customBinaryTreeChecker::load(std::string &str) {
    _vocabulary->Add(str);
}

customBinaryTreeChecker::~customBinaryTreeChecker() {
    delete _vocabulary;
    delete _non_find;
}