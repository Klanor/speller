#ifndef CASTBINARYTREE
#define CASTBINARYTREE

#include "../BaseChecker.h"
#include "../UsingStructures/BinaryTree/BinarySearch.h"

class customBinaryTreeChecker : public Checker {
    BinarySearchTree *_vocabulary;
    BinarySearchTree *_non_find;

    void check(std::string &str) override;

    void load(std::string &str) override;

public:
    customBinaryTreeChecker() :
            _vocabulary(new BinarySearchTree()), _non_find(new BinarySearchTree()) {};

    ~customBinaryTreeChecker() override;
};

#endif // !CastBinaryTree