#include "stdMap.h"

size_t stdHashMapChecker::hash_func(std::string &buf) {
    std::hash<std::string> key;
    return key(buf);
}

void stdHashMapChecker::check(std::string &str) {
    ++parameters.sum_check_word;
    if (_vocabulary->find(hash_func(str)) == _vocabulary->end()) {
        ++parameters.sum_uncorrect_word;
        _non_find->emplace(hash_func(str), str);

    }
}

void stdHashMapChecker::load(std::string &str) {
    _vocabulary->emplace(hash_func(str), str);
}

stdHashMapChecker::~stdHashMapChecker() {
    delete _vocabulary;
    delete _non_find;
}




