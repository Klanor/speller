#ifndef STDHASHMAP
#define STDHASHMAP

#include "../BaseChecker.h"
#include <map>


class stdHashMapChecker : public Checker {
    std::map<size_t, std::string> *_vocabulary;
    std::map<size_t, std::string> *_non_find;

    static size_t hash_func(std::string &buf);

    void check(std::string &str) override;

    void load(std::string &str) override;

public:
    stdHashMapChecker() :
            _vocabulary(new std::map<size_t, std::string>()), _non_find(new std::map<size_t, std::string>()) {};

    ~stdHashMapChecker() override;
};

#endif // !STDHASHMAP





