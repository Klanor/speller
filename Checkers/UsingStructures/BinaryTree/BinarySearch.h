#ifndef BINARY_SEARCH
#define BINARY_SEARCH

#include "../../BaseChecker.h"

class BinarySearchTree {
    class Node {
        std::string _val;
        size_t _key;
        Node *_left;
        Node *_right;

        Node(std::string v, size_t k) : _val(std::move(v)), _key(k), _left(nullptr), _right(nullptr) {};

        ~Node() {
            delete _left;
            delete _right;
        }

        friend class BinarySearchTree;
    };

    Node *node;
    std::hash<std::string> hash_fun;
    bool empty;

public:
    BinarySearchTree() : node(new Node(std::string(), 0)), empty(true) {};

    explicit BinarySearchTree(std::string &str) : node(new Node(str, hash_fun(str))), empty(false) {};

    void Add(std::string &str);

    bool find(std::string &str);

    ~BinarySearchTree() {
        delete node;
    };
};


#endif
