#include "BinarySearch.h"

void BinarySearchTree::Add(std::string &str) {
    size_t key = hash_fun(str);
    if (empty) {
        node->_key = key;
        node->_val = str;
        empty = false;
    } else {
        Node *curr_node = node;
        while (true) {
            if (curr_node->_key > key) {
                if (curr_node->_left == nullptr)
                    curr_node->_left = new Node(str, key);
                else
                    curr_node = curr_node->_left;
            } else if (curr_node->_key < key) {
                if (curr_node->_right == nullptr)
                    curr_node->_right = new Node(str, key);
                else
                    curr_node = curr_node->_right;
            } else
                break;
        }
    }
}

bool BinarySearchTree::find(std::string &str) {
    size_t key = hash_fun(str);
    Node *curr_node = node;
    while (curr_node != nullptr) {
        if (curr_node->_key > key)
            curr_node = curr_node->_left;
        else if (curr_node->_key < key)
            curr_node = curr_node->_right;
        else
            return true;
    }
    return false;
}

