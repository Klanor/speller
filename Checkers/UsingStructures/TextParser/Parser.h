#ifndef SPELLER_PARSER_H
#define SPELLER_PARSER_H

#include <string>
#include <algorithm>

struct Parser {
    static bool
    LineCleaner(std::string &line);//преобразует строку считаную из текста удаляя из нее все кроме букв и апостофа

    static std::string WordParser(std::string &line, size_t &offset);//парсит строку в отдельные слова
};

#endif //SPELLER_PARSER_H
