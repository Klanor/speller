#include "Parser.h"
#include <climits>

bool Parser::LineCleaner(std::string &line) {

    for (auto j = line.begin(); j != line.end(); ++j) {

        if (*j == ' ' || *j == '\'') {
            if (*(j + 1) == ' ' || *(j - 1) == ' ')
                line.erase(j--);
        } else {
            if (*j < 'a' || *j > 'z') {
                if (*j >= 'A' && *j <= 'Z')
                    *j += ' ';
                else
                    line.erase(j--);
            }
        }
    }

    while (*line.begin() == ' ' || *line.begin() == '\'')
        line.erase(line.begin());

    while (*(line.end() - 1) == ' ' || *(line.end() - 1) == '\'')
        line.pop_back();

    return line.empty();
}

std::string Parser::WordParser(std::string &line, size_t &offset) {
    size_t prob = line.find(' ', offset);
    auto beg = line.begin() + offset;
    std::string::iterator end;

    if (prob == std::string::npos) {
        end = line.end();
        offset = 0;
    } else {
        end = line.begin() + prob++;
        offset = prob;
    }

    return std::string(beg, end);
}
