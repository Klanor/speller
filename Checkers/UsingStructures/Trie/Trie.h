#ifndef SPELLER_TRIE_H
#define SPELLER_TRIE_H

#include "../../BaseChecker.h"

class Trie {
    struct prefix {
        bool _end_of_word;
        char _prefix;
        std::vector<prefix *> *child;

        explicit prefix(char pref = ' ') :
                _end_of_word(false), _prefix(pref), child(new std::vector<prefix *>(27)) {};

        ~prefix() { delete child; }
    };

    std::vector<prefix *> *node;

    static size_t _insert(const char &ch);

public:
    Trie() : node(new std::vector<prefix *>(27)) {};

    void add(std::string &value);

    bool find(std::string &value);

    ~Trie() { delete node; }
};


#endif //SPELLER_TRIE_H
