#include "Trie.h"

size_t Trie::_insert(const char &ch) {

    if (ch == '\'')
        return 26;
    return ch - 'a';
}

void Trie::add(std::string &value) {
    size_t tmp = _insert(value[0]);
    prefix *cur;
    if (node->at(tmp) == nullptr) {
        node->at(tmp) = new prefix(value[0]);
    }
    cur = node->at(tmp);

    for (auto i = value.begin() + 1; i != value.end(); ++i) {
        tmp = _insert(*i);

        if (cur->child->at(tmp) == nullptr)
            cur->child->at(tmp) = new prefix(*i);

        cur = cur->child->at(tmp);
    }
    cur->_end_of_word = true;
}

bool Trie::find(std::string &value) {
    size_t tmp = _insert(value[0]);
    prefix *cur;
    if (node->at(tmp) == nullptr) {
        return false;
    }
    cur = node->at(tmp);

    for (auto i = value.begin() + 1; i != value.end(); ++i) {
        tmp = _insert(*i);

        if (cur->child->at(tmp) == nullptr)
            return false;

        cur = cur->child->at(tmp);
    }
    return cur->_end_of_word;
}
