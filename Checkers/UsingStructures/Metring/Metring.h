#ifndef SPELLER_METRING_H
#define SPELLER_METRING_H

#include <chrono>

namespace ct = std::chrono;

class SearchParameters {

    class Time {
        time_t time;
        ct::time_point<std::chrono::steady_clock> start;
    public:
        Time() : time(0), start() {};

        void Start();//запускает таймер
        void Stop();//останавливает таймер время записывает в time
        [[nodiscard]] time_t get() const;
    };

    class Counter {
        unsigned int sum;
    public:
        Counter() : sum(0) {};

        Counter &operator++();

        [[nodiscard]] unsigned int get() const;
    };

public:
    Time vocabulary_download_time;
    Time check_time;
    Counter sum_check_word;
    Counter sum_uncorrect_word;

    SearchParameters() : vocabulary_download_time(), check_time(), sum_check_word(), sum_uncorrect_word() {};
};

#endif //SPELLER_METRING_H
