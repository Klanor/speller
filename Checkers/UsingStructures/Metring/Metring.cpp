#include "Metring.h"

time_t SearchParameters::Time::get() const {
    return time;
}

void SearchParameters::Time::Start() {
    start = ct::steady_clock::now();
}

void SearchParameters::Time::Stop() {
    auto dr = ct::duration_cast<ct::milliseconds>(ct::steady_clock::now() - start);
    time += dr.count();
}

SearchParameters::Counter &SearchParameters::Counter::operator++() {
    this->sum += 1;
    return *this;
}

unsigned int SearchParameters::Counter::get() const {
    return sum;
}
