#include "stdVector.h"

stdVectorChecker::stdVectorChecker() {
    _vocabulary = new std::vector<std::string>;
    _vocabulary->reserve(200000);

    _non_find = new std::vector<std::string>;
    _non_find->reserve(50000);
}

void stdVectorChecker::load(std::string &str) {
    _vocabulary->push_back(str);
}

void stdVectorChecker::check(std::string &str) {
    ++parameters.sum_check_word;
    if (!std::binary_search(_vocabulary->begin(), _vocabulary->end(), str)) {
        ++parameters.sum_uncorrect_word;
        _non_find->push_back(str);
    }
}

stdVectorChecker::~stdVectorChecker() {
    delete _vocabulary;
    delete _non_find;
}


