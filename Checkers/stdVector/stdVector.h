#ifndef STDVECTOR
#define STDVECTOR

#include "../BaseChecker.h"

class stdVectorChecker : public Checker {
    std::vector<std::string> *_vocabulary;
    std::vector<std::string> *_non_find;

    void check(std::string &str) override;

    void load(std::string &str) override;

public:
    stdVectorChecker();

    ~stdVectorChecker() override;
};

#endif // !STDVECTOR