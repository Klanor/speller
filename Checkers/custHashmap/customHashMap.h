#ifndef CASTHASHMAP
#define CASTHASHMAP

#include "../BaseChecker.h"
#include "../UsingStructures/BinaryTree/BinarySearch.h"

class customHashMapChecker : public Checker {
    std::vector<BinarySearchTree> *_vocabulary;
    std::vector<BinarySearchTree> *_non_find;

    void check(std::string &str) override;

    void load(std::string &str) override;

public:
    customHashMapChecker() :
            _vocabulary(new std::vector<BinarySearchTree>(26)), _non_find(new std::vector<BinarySearchTree>(26)) {};

    ~customHashMapChecker() override;
};

#endif // !CASTHASHMAP

