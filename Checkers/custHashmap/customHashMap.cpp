#include "customHashMap.h"

void customHashMapChecker::check(std::string &str) {
    ++parameters.sum_check_word;
    unsigned int tmp = str[0] - 'a';
    if (!_vocabulary->at(tmp).find(str)) {
        _non_find->at(tmp).Add(str);
        ++parameters.sum_uncorrect_word;
    }
}

void customHashMapChecker::load(std::string &str) {
    _vocabulary->at(str[0] - 'a').Add(str);
}

customHashMapChecker::~customHashMapChecker() {
    delete _vocabulary;
    delete _non_find;
}





