#include "customTrie.h"

void customTrie::check(std::string &str) {
    ++parameters.sum_check_word;
    if (!_vocabulary->find(str)) {
        _non_find->add(str);
        ++parameters.sum_uncorrect_word;
    }
}

void customTrie::load(std::string &str) {
    _vocabulary->add(str);
}

customTrie::~customTrie() {
    delete _vocabulary;
    delete _non_find;
}

