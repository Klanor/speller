#ifndef SPELLER_CUSTOMTRIE_H
#define SPELLER_CUSTOMTRIE_H

#include "../BaseChecker.h"
#include "../UsingStructures/Trie/Trie.h"

class customTrie : public Checker {
    Trie *_vocabulary;
    Trie *_non_find;

    void check(std::string &str) override;

    void load(std::string &str) override;

public:
    customTrie() : _vocabulary(new Trie()), _non_find(new Trie()) {};

    ~customTrie() override;

};

#endif //SPELLER_CUSTOMTRIE_H
