#ifndef HEADER
#define HEADER

#include "UsingStructures/Metring/Metring.h"
#include "UsingStructures/TextParser/Parser.h"
#include <fstream>
#include <vector>
#include <exception>

static std::exception_ptr global_ex = nullptr;

class Checker {
protected:

    std::ifstream in;

    virtual void check(std::string &str) = 0;

    /*виртуальный метод для проверки и добавления слов в соварь, которые считывает из текстов
    метод AddText(35)*/

    virtual void load(std::string &str) = 0;
    //виртуальный метод для добавления строк в словарь считаных методом AddVocabulary(32)

public:

    SearchParameters parameters;
    //поле для получения возможности измерять свойства. Определен в Metring

    Checker() : parameters() {};

    void AddVocabulary(const char *voc);
    //читает файл словаря по указаному пути

    void AddText(const char *text);
    //читает файл текста, вызываемые методы TextParser-а парсят строку в отдельные слова, которые передаються в check(19)

    virtual ~Checker() {};
};

#endif // !HEADER

