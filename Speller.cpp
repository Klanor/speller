#include "Checkers/custBintree/customBinaryTree.h"
#include "Checkers/custHashmap/customHashMap.h"
#include "Checkers/custTrie/customTrie.h"
#include "Checkers/stdMap/stdMap.h"
#include "Checkers/stdVector/stdVector.h"
#include <iostream>
#include <thread>
#include <filesystem>

const char *vocabulary_path("../large.txt");//путь к словарю
const char *directory_text_path("../text");//путь к папке с текстами

void startRead(const std::vector<std::string> &file_list, Checker &_ins) {

    _ins.AddVocabulary(vocabulary_path);

    for (const auto &i : file_list) {
        _ins.AddText(i.c_str());
        std::cout << typeid(_ins).name() << " done " << i << std::endl;
    }
}

void coutResults(const Checker &_ins) {
    std::cout << typeid(_ins).name() << " "
              << _ins.parameters.vocabulary_download_time.get() << " "
              << _ins.parameters.check_time.get() << " "
              << _ins.parameters.sum_check_word.get() << " "
              << _ins.parameters.sum_uncorrect_word.get() << std::endl;
}

int main() {
    std::vector<std::string> filesList;

    namespace fs = std::filesystem;

    if (!fs::exists(directory_text_path))
        fs::create_directory(directory_text_path);

    fs::path p(directory_text_path);

    for (auto i = fs::directory_iterator(p); i != fs::directory_iterator(); ++i) {
        if (!is_directory(i->path()))
            filesList.push_back("../text/" + i->path().filename().string());
    }
    if (filesList.empty()) {
        std::cout << "Directory " << directory_text_path << " empty now" << std::endl;
        return 0;
    }

    auto *vec = new stdVectorChecker();
    auto *map = new stdHashMapChecker();
    auto *tree = new customBinaryTreeChecker();
    auto *cmap = new customHashMapChecker();
    auto *trie = new customTrie();

    std::thread tr1([&vec, &filesList] { startRead(filesList, *vec); });

    std::thread tr2([&map, &filesList] { startRead(filesList, *map); });

    std::thread tr3([&tree, &filesList] { startRead(filesList, *tree); });

    std::thread tr4([&cmap, &filesList] { startRead(filesList, *cmap); });

    startRead(filesList, *trie);

    tr1.join();
    tr2.join();
    tr3.join();
    tr4.join();

    if (global_ex) {
        try {
            std::rethrow_exception(global_ex);
        } catch (std::exception &ex) {
            std::cout << ex.what() << std::endl;
            return 0;
        }
    }

    std::cout << "__________-----***RESULT***-----__________" << std::endl;
    std::cout << "|NAME|DOWNLOAD TIME|CHECK Time|SUM OF ALL WORD|SUM OF UNCORRECT WORD|" << std::endl;
    coutResults(*vec);
    coutResults(*map);
    coutResults(*tree);
    coutResults(*cmap);
    coutResults(*trie);

    return 1;
}